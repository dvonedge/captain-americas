Function Parametros_Robot(velocidad As Integer, aceleracion As Integer, AltoPoder As Boolean)
	Real p
	If ErrorOn Then Reset							'reiniciar en caso de error
	If Motor = Off Then Motor On					'encender motores
	If AltoPoder Then Power High Else Power Low		'alto poder o bajo poder

	Speed velocidad
	Accel aceleracion, aceleracion
	
	LimZ 0			'limite en altura para los jumps
	
	'precision de los movimientos
	p = 0
	Fine p, p, p, p
	
	
Fend
