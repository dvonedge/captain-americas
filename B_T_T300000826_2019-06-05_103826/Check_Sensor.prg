Global Boolean activarcontador, msgSensorON, msgSensor2ON, msgFault, msgTrackJam_ON, msgCarrierMotion_ON, msgEPD_ON, msgLowCarrier_ON, msgLowCover_ON
Global Integer cantidaderror
Function Sensors

	activarcontador = False
	msgFault = False
	msgSensorON = False
	msgSensor2ON = False
  	msgTrackJam_ON = False
  	msgCarrierMotion_ON = False
  	msgEPD_ON = False
  	msgLowCarrier_ON = False
  	msgLowCover_ON = False
	Wait 0.6
	If Sw(Sensor) And i >= 7 Then
			msgFault = True
			Alarma = True
			cantidaderror = cantidaderror + 1
			Pause
			If msgSensorON = False Then
				Xqt Sensor_ON, NoPause
				msgSensorON = True
			EndIf
			
		EndIf
		If SenErrDetected = True Then
			SenErrDetected = False
			msgFault = True
			cantidaderror = cantidaderror + 1
			Pause
			If msgSensor2ON = False Then
				Xqt Sensor2_ON, NoPause
				msgSensor2ON = True
			EndIf
			Alarma = True
		EndIf
		If Sw(Sensor2) And i >= 13 Then
			SenErrDetected = True
		EndIf
		
		If Sw(Fault) Then
			msgFault = True
			If Sw(TrackJam) = False Then
				cantidaderror = cantidaderror + 1
				Pause
				If msgTrackJam_ON = False Then
					Xqt Track_Jam, NoPause
					msgTrackJam_ON = True
				EndIf
				Alarma = True
			
			
			
			ElseIf Sw(LowCarrier) Then
				cantidaderror = cantidaderror + 1
				Pause
				If msgLowCarrier_ON = False Then
					Xqt Low_Carrier, NoPause
					msgLowCarrier_ON = True
				EndIf
				Alarma = True
			
			
			ElseIf Sw(LowCover) Then
			    cantidaderror = cantidaderror + 1
				Pause
				If msgLowCover_ON = False Then
					Xqt Low_Cover, NoPause
					msgLowCover_ON = True
				EndIf
				Alarma = True
			
			ElseIf Sw(EPDSignal) And EPD_ON = True Then
				cantidaderror = cantidaderror + 1
				Pause
				If msgEPD_ON = False Then
					Xqt EPD_Signal, NoPause
					msgEPD_ON = True
				EndIf
				Alarma = True
			EndIf
		EndIf
Fend

