Global Boolean msgFin
Function Fin
'	Integer r
'	r = 0
  	MemOn End_Trabajo
  	msgFin = False
  	MemOff run_program
  	Off LuzCamara
  	Call AgregarDatos
'  	Do While r < 20
'  		On Advance
'		Wait 0.05
'		Off Advance
'		Call Sensors
'		r = r + 1
'  	Loop
  	
  	Do While MemSw(End_Trabajo) = True
  		If msgFin = False Then
  			Xqt FinTrabajo
  			msgFin = True
  		EndIf
  		On LuzVerde
  		On Buzzer
  		Wait 0.5
  		Off LuzVerde
  		Off Buzzer
  		Wait 0.5
  	Loop
  	
Fend
Function FinTrabajo
	String msg$, title$
	Integer mFlags, answer
	msg$ = "Trabajo Finalizado" + CRLF
  	title$ = "Proceso Terminado"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msg$, mFlags, title$, answer
  
	If answer = IDOK Then
		Off Buzzer
		Off LuzVerde
		Off LuzAmarilla
		Quit All
	EndIf
	
Fend
Integer fileNum
Function AgregarDatos
	fileNum = FreeFile
'	WOpen "DatosProduccion.txt" As #fileNum
'	Close #fileNum
	AOpen "DatosProduccion.txt" As #fileNum
		Print #fileNum, "***************************************************************************************"
		Print #fileNum, Date$, " Numero de Orden: ", GetNumeroOrden$, " Cantidad de Piezas: ", cantpiezas
		Print #fileNum, "Se detuvo: ", cantidaderror, " veces ", "Tiempo total: ", Tmr(0), "segundos"
		
Fend

