Global Boolean activarcontador, msgSensorON, msgSensor2ON, msgFault, msgTrackJam_ON, msgCarrierMotion_ON, msgEPD_ON, msgLowCarrier_ON, msgLowCover_ON
Function Error_Sensor
	espera = 0
	activarcontador = False
	msgFault = False
	msgSensorON = False
	msgSensor2ON = False
  	msgTrackJam_ON = False
  	msgCarrierMotion_ON = False
  	msgEPD_ON = False
  	msgLowCarrier_ON = False
  	msgLowCover_ON = False
	Do While MemSw(run_program)
		If Sw(EPDSignal) = False And EPD_ON = False Then
			EPD_ON = True
		EndIf
		If espera = 0 Then
			EPD_ON2 = True
		EndIf
		If Sw(Sensor) And LockRead = False And EPD_ON2 = True Then
			msgFault = True
			Pause
			If msgSensorON = False Then
				Xqt Sensor_ON, NoPause
				msgSensorON = True
			EndIf
			On LuzAmarilla
			On Buzzer
			Wait 0.5
			Off Buzzer
			Off LuzAmarilla
			Wait 0.5
		EndIf
		If Sw(Sensor2) And LockRead = False And EPD_ON2 = True Then
			msgFault = True
			Pause
			If msgSensor2ON = False Then
				Xqt Sensor2_ON, NoPause
				msgSensor2ON = True
			EndIf
			On LuzAmarilla
			On Buzzer
			Wait 0.5
			Off Buzzer
			Off LuzAmarilla
			Wait 0.5
		EndIf
		If Sw(Fault) Then
			msgFault = True
			If Sw(TrackJam) = False Then
				Pause
				If msgTrackJam_ON = False Then
					Xqt Track_Jam, NoPause
					msgTrackJam_ON = True
				EndIf
				On LuzAmarilla
				On Buzzer
				Wait 0.5
				Off Buzzer
				Off LuzAmarilla
				Wait 0.5
			
			'ElseIf Sw(CarrierMotion) = False Then
			'	Pause
		'		If msgCarrierMotion_ON = False Then
		'			Xqt Carrier_Motion, NoPause
		'			msgCarrierMotion_ON = True
		'		EndIf
		'		On LuzAmarilla
	'			On Buzzer
	'			Wait 0.5
	'			Off Buzzer
	'			Off LuzAmarilla
	'			Wait 0.5
			
			
			ElseIf Sw(LowCarrier) Then
				Pause
				If msgLowCarrier_ON = False Then
					Xqt Low_Carrier, NoPause
					msgLowCarrier_ON = True
				EndIf
				On LuzAmarilla
				On Buzzer
				Wait 0.5
				Off Buzzer
				Off LuzAmarilla
				Wait 0.5
			
			
			ElseIf Sw(LowCover) Then
				Pause
				If msgLowCover_ON = False Then
					Xqt Low_Cover, NoPause
					msgLowCover_ON = True
				EndIf
				On LuzAmarilla
				On Buzzer
				Wait 0.5
				Off Buzzer
				Off LuzAmarilla
				Wait 0.5
			
			ElseIf Sw(EPDSignal) And EPD_ON = True Then
				Pause
				If msgEPD_ON = False Then
					Xqt EPD_Signal, NoPause
					msgEPD_ON = True
				EndIf
				On LuzAmarilla
				On Buzzer
				Wait 0.5
				Off Buzzer
				Off LuzAmarilla
				Wait 0.5
			EndIf
		EndIf
		
	Loop
Fend
Function Track_Jam
	String msgTrackJam$, titleTrackJam$
	Integer mFlags, answer
	msgTrackJam$ = "PIEZA MAL COLODA" + CRLF
 	msgTrackJam$ = msgTrackJam$ + "FAVOR REVISAR PIEZA EN EL CARRIER, PARA CONTINUAR"
  	titleTrackJam$ = "MAL COLOCACIÓN"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgTrackJam$, mFlags, titleTrackJam$, answer
  	If answer = IDOK Then
  		msgTrackJam_ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
  	
Fend
Function Carrier_Motion
	String msgCarrierMotion$, titleCarrierMotion$
	Integer mFlags, answercarrier
	msgCarrierMotion$ = "PROBLEMA EN EL CARRIER" + CRLF
 	msgCarrierMotion$ = msgCarrierMotion$ + "FAVOR REVISAR EL CARRIER, PARA CONTINUAR"
  	titleCarrierMotion$ = "PROBLEMA EN EL CARRIER"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgCarrierMotion$, mFlags, titleCarrierMotion$, answercarrier
  	If answercarrier = IDOK Then
  		msgCarrierMotion_ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend
Function EPD_Signal
	String msgEPD$, titleEPD$
	Integer mFlags, answerEPD
	msgEPD$ = "CAVIDAD VACIA" + CRLF
 	msgEPD$ = msgEPD$ + "FAVOR REVISAR CAVIDAD VACIA EN EL CARRIER, PARA CONTINUAR"
  	titleEPD$ = "CAVIDAD VACIA"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgEPD$, mFlags, titleEPD$, answerEPD
  	If answerEPD = IDOK Then
  		msgEPD_ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend
Function Low_Carrier
	String msgLowCarrier$, titleLowCarrier$
	Integer mFlags, answerLCarrier
	msgLowCarrier$ = "CARRIER AGOTADO" + CRLF
 	msgLowCarrier$ = msgLowCarrier$ + "FAVOR CAMBIAR CARRIER, PARA CONTINUAR"
  	titleLowCarrier$ = "CARRIER AGOTADO"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
	
	MsgBox msgLowCarrier$, mFlags, titleLowCarrier$, answerLCarrier
	If answerLCarrier = IDOK Then
  		msgLowCarrier_ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend
Function Low_Cover
	String msgLowCover$, titleLowCover$
	Integer mFlags, answerLCover
	msgLowCover$ = "COVER CASI AGOTADO" + CRLF
 	msgLowCover$ = msgLowCover$ + "FAVOR CAMBIAR COVER, PARA CONTINUAR"
  	titleLowCover$ = "COVER AGOTADO"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgLowCover$, mFlags, titleLowCover$, answerLCover
  	If answerLCover = IDOK Then
  		msgLowCover_ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend
Function Sensor_ON
	String msgSensor$, titleSensor$
	Integer mFlags, answerSensor
	msgSensor$ = "PIEZA MAL COLOCADA EN EL CARRIER" + CRLF
 	msgSensor$ = msgSensor$ + "FAVOR REPARAR LA PIEZA" + CRLF
 	msgSensor$ = msgSensor$ + "PRESIONAR OK, PARA CONTINUAR"
    titleSensor$ = "PIEZA MAL COLOCADA"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgSensor$, mFlags, titleSensor$, answerSensor
  	If answerSensor = IDOK Then
  		msgSensorON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend
Function Sensor2_ON
	String msgSensor2$, titleSensor2$
	Integer mFlags, answerSensor2
	msgSensor2$ = "PIEZA MAL COLOCADA EN EL CARRIER" + CRLF
 	msgSensor2$ = msgSensor2$ + "FAVOR REPARAR LA PIEZA" + CRLF
 	msgSensor2$ = msgSensor2$ + "PRESIONAR OK, PARA CONTINUAR"
    titleSensor2$ = "PIEZA MAL COLOCADA"
  	mFlags = MB_OK + MB_ICONEXCLAMATION
  	
  	MsgBox msgSensor2$, mFlags, titleSensor2$, answerSensor2
  	If answerSensor2 = IDOK Then
  		msgSensor2ON = False
  		msgFault = False
  		Alarma = False
  	EndIf
Fend

