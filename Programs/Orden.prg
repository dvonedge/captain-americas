Global Integer cantpiezas
Global String GetNumeroOrden$, GetCantidadOrden$

Function GetOrder
	String msg$, title$
	Integer mFlags, answer
	Jump Phome
	Call GetNumeroOrdens$
	msg$ = "N�mero de orden: " + GetNumeroOrden$ + CRLF
	msg$ = msg$ + "Cantidad de piezas: " + GetCantidadOrden$ + CRLF
	msg$ = msg$ + "Continuar?"
	title$ = "CONFIRMACI�N"
	mFlags = MB_YESNO + MB_ICONQUESTION
	MsgBox msg$, mFlags, title$, answer
	If answer = IDYES Then
		Call InicioPrograma
	ElseIf answer = IDNO Then
		Call GetOrder
	EndIf
Fend
Function GetNumeroOrdens$ As String
	String prompt$, title$, data$
	prompt$ = "Ingrese el n�mero de orden:"
	title$ = "Identificaci�n de Orden"
	InputBox prompt$, title$, "", data$
	If data$ <> "@" Then
		GetNumeroOrden$ = data$
		Call GetCantidadOrdens$
	ElseIf data$ = "@" Then
		Quit All
	EndIf
Fend
Function GetCantidadOrdens$ As String
	String prompt$, title$, data$, answer$
	prompt$ = "Ingrese la cantidad de piezas:"
	title$ = "Cantidad de piezas de la Orden"
	InputBox prompt$, title$, "", data$
	If data$ <> "@" Then
		GetCantidadOrden$ = data$
		cantpiezas = Val(GetCantidadOrden$)
	ElseIf data$ = "@" Then
		Quit All
	EndIf

Fend
