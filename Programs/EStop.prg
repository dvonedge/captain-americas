Function EmergencyStop
	Xqt EmergencyStop2, NoEmgAbort
Fend
Global Boolean msgEStop_On
Function EmergencyStop2
	String msg$, title$
	Integer mFlags, answer
	msg$ = "BOTON DE EMERGENCIA PRESIONADO" + CRLF
  	title$ = "ADVERTENCIA"
  	mFlags = MB_OK + MB_ICONSTOP
	msgEStop_On = False
	Wait EStopOn
	
	Do While EStopOn
		
		Off LuzVerde, Forced
		Off LuzAmarilla, Forced
		Off LuzCamara, Forced
		Off Aire, Forced
		Off Vacio, Forced
		Off Advance, Forced
		On LuzRoja, Forced
		On Buzzer, Forced
		Wait 0.5
		Off Buzzer, Forced
		Off LuzRoja, Forced
		Wait 0.5
	Loop
	MsgBox msg$, mFlags, title$, answer
	If answer = IDOK Then
  			Off Buzzer
  	EndIf
Fend

