Global Boolean msgPause_ON
Function Inter_Lock
		msgPause_ON = False
		Do While MemSw(run_program)
		
			If Sw(Interlock) = Off And msgFault = False Then
				Pause
				If msgPause_ON = False Then
					Xqt MsgPause, NoPause
					msgPause_ON = True
				EndIf
				On LuzAmarilla
				On Buzzer
				Wait 0.5
				Off Buzzer
				Off LuzAmarilla
				Wait 0.5
			EndIf
		Loop
	
Fend
Function MsgPause
	String msg$, title$
	Integer mFlags, answer
	msg$ = "PUERTA ABIERTA" + CRLF
 	msg$ = msg$ + "FAVOR CERRAR PUERTA, PARA CONTINUAR"
  	title$ = "Proceso Interrumpido"
  	mFlags = MB_OKCANCEL + MB_ICONEXCLAMATION
  	
  	MsgBox msg$, mFlags, title$, answer
  		
  	If answer = IDOK Then
  		msgPause_ON = False
  	EndIf
	If answer = IDCANCEL Then
		Quit All
	EndIf
Fend

